package com.crud.react.DTO;

public class ProdukDto {

    private String nama_makanan;

    private String deskripsi;

    private Integer harga;

    public ProdukDto(String nama_makanan, String deskripsi, Integer harga) {
        this.nama_makanan = nama_makanan;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }
}
