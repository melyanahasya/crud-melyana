package com.crud.react.Service;

import com.crud.react.model.ProdukImage;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProdukImageService {
    List<ProdukImage> findAll();

    ProdukImage addImage(Integer produkId, MultipartFile multipartFile);
}
