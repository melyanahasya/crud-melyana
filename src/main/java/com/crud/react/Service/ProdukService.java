package com.crud.react.Service;

import com.crud.react.DTO.ProdukDto;
import com.crud.react.model.Produk;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ProdukService {
    List<Produk> getAllProduk();

    Produk addProduk(MultipartFile multipartFile, ProdukDto produkDto);

    Produk getProduk(Integer Id);

    Produk editProduk(Integer Id, String gambar, String nama_makanan, String deskripsi, Integer harga);

    void deleteProdukById(Integer Id);
}
