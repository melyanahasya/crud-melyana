package com.crud.react.Service;

import com.crud.react.DTO.LoginDto;
import com.crud.react.enumed.UserType;
import com.crud.react.model.Users;

import java.util.List;
import java.util.Map;

public interface UsersService {

    Map<String, Object> login (LoginDto loginDto);

    List<Users> getAllUsers();

    Users addUsers(Users users);

    Users getUsers(Integer Id);

    Users editUsers(Integer Id, String email, String password, UserType user_type);

    void deleteUsersById(Integer Id);
}
