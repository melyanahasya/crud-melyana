package com.crud.react.controller;

import com.crud.react.DTO.ProdukDto;
import com.crud.react.Service.ProdukService;
import com.crud.react.model.Produk;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/Produk")
public class ProdukController {
   @Autowired
   private ProdukService produkService;

    @GetMapping("/all")
    public CommonResponse<List<Produk>> getAllProduk(){
        return ResponHelper.ok(produkService.getAllProduk());
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Produk> addProduk(@RequestPart("file") MultipartFile multipartFile, ProdukDto produkDto) {
        return ResponHelper.ok(produkService.addProduk(multipartFile, produkDto));
    }

    @GetMapping("/{Id}")
    public CommonResponse<Produk> getProduk(@PathVariable("Id") Integer Id) {
        return ResponHelper.ok(produkService.getProduk(Id));
    }

    @PutMapping("/{Id}")
    public CommonResponse<Produk> editProdukById(@PathVariable("Id") Integer Id, @RequestBody Produk produk) {
        return ResponHelper.ok(produkService.editProduk(Id, produk.getGambar(), produk.getNama_makanan(), produk.getDeskripsi(), produk.getHarga()));
    }

    @DeleteMapping("/{Id}")
    public void deleteProdukById(@PathVariable("Id") Integer Id) {
        produkService.deleteProdukById(Id);
    }
}
