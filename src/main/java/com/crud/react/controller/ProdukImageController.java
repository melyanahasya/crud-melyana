package com.crud.react.controller;

import com.crud.react.Service.ProdukImageService;
import com.crud.react.model.ProdukImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/image")
public class ProdukImageController {

    @Autowired
    ProdukImageService produkImageService;

    @PostMapping(consumes = "multipart/form-data")
    public ProdukImage addImage(@RequestPart("produkId") String produkId, @RequestPart("file")MultipartFile multipartFile) {
        return produkImageService.addImage(Integer.valueOf(produkId), multipartFile);
    }
  @GetMapping
  public List<ProdukImage> getAll() {
        return produkImageService.findAll();
  }
}
