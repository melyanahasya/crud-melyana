package com.crud.react.controller;

import com.crud.react.DTO.LoginDto;
import com.crud.react.DTO.UsersDto;
import com.crud.react.Service.UsersService;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponHelper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/users")
public class UsersController {
    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String,Object>> login(@RequestBody LoginDto loginDto) {
        return ResponHelper.ok(usersService.login(loginDto));
    }

    @PostMapping("/sign-up")
    public CommonResponse<Users> addUsers(@RequestBody UsersDto usersDto) {
        return ResponHelper.ok(usersService.addUsers(modelMapper.map(usersDto, Users.class)));
    }

    @GetMapping("/all")
    public CommonResponse<List<Users>> getAllUsers() {
       return ResponHelper.ok(usersService.getAllUsers());
    }

    @GetMapping("/{Id}")
    public CommonResponse<Users> getUsers(@PathVariable("Id") Integer Id) {
        return ResponHelper.ok(usersService.getUsers(Id));
    }

    @PostMapping
    public CommonResponse<Users> addUsers(@RequestBody Users users) {
        return ResponHelper.ok(usersService.addUsers(users));
    }

   @PutMapping("/{Id}")
    public CommonResponse<Users> editUsersById(@PathVariable("Id") Integer Id, @RequestBody Users users) {
       return ResponHelper.ok(usersService.editUsers(Id, users.getEmail(), users.getPassword(), users.getRole()));
   }

   @DeleteMapping("/{Id}")
    public void deleteUsersById(@PathVariable("Id") Integer Id) {
        usersService.deleteUsersById(Id);
   }
}
