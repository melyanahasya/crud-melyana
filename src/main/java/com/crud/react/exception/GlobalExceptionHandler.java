package com.crud.react.exception;

import com.crud.react.response.ResponHelper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundexception.class)
    public ResponseEntity<?> notFound(ChangeSetPersister.NotFoundException notFoundException) {
        return ResponHelper.error(notFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InternalErrorException.class)
    public ResponseEntity<?> internalEeror(InternalErrorException InternalErrorException) {
        return ResponHelper.error(InternalErrorException.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
