package com.crud.react.exception;

public class NotFoundexception extends RuntimeException {
    public NotFoundexception(String message) {
        super(message);
    }
}
