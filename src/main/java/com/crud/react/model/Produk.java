package com.crud.react.model;
import javax.persistence.*;

@Entity
@Table(name = "produk")
public class Produk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Lob
    @Column(name = "gambar")
    private String gambar;

    @Column(name = "nama_makanan")
    private String nama_makanan;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "harga")
    private Integer harga;


    public Produk(String gambar, String nama_makanan, String deskripsi, Integer harga) {
        this.gambar = gambar;
        this.nama_makanan = nama_makanan;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNama_makanan() {
        return nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Produk{" +
                "Id=" + Id +
                ", gambar='" + gambar + '\'' +
                ", nama_makanan='" + nama_makanan + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", harga=" + harga +
                '}';
    }

    public Produk() {
    }
}
