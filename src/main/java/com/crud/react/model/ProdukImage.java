package com.crud.react.model;

import javax.persistence.*;

@Entity
@Table(name = "produk_image")
public class ProdukImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Lob
    @Column(name = "image_url")
    private String imageUrl;


    public ProdukImage() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ProdukImage(String imageUrl, Produk produkId) {
        this.imageUrl = imageUrl;
    }
}
