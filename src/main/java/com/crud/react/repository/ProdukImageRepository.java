package com.crud.react.repository;

import com.crud.react.model.ProdukImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdukImageRepository extends JpaRepository<ProdukImage, Integer> {
}
