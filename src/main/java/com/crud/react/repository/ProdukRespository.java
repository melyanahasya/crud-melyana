package com.crud.react.repository;

import com.crud.react.model.Produk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdukRespository extends JpaRepository<Produk, Integer> {
}
