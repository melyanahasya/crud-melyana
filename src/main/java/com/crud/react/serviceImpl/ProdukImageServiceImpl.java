package com.crud.react.serviceImpl;

import com.crud.react.Service.ProdukImageService;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundexception;
import com.crud.react.model.Produk;
import com.crud.react.model.ProdukImage;
import com.crud.react.repository.ProdukImageRepository;
import com.crud.react.repository.ProdukRespository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;


@Service
public class ProdukImageServiceImpl implements ProdukImageService {
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/tugasfirebase-b061bappspot.com/o/%s?alt=media";

    @Autowired
    ProdukRespository produkRespository;

    @Autowired
    ProdukImageRepository produkImageRepository;

    @Override
    public List<ProdukImage> findAll() {
        return produkImageRepository.findAll();
    }

    @Override
    public ProdukImage addImage(Integer produkId, MultipartFile multipartFile) {
        Produk produk = produkRespository.findById(produkId).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan"));
        String url = imageConverter(multipartFile);
        ProdukImage produkImage = new ProdukImage(url, produk);
        return produkImageRepository.save(produkImage);
    }


    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile (file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }


    private String uploadFile(File file, String fileName) throws IOException{
        BlobId blobId = BlobId.of("tugasfirebase-b061b.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }
}
