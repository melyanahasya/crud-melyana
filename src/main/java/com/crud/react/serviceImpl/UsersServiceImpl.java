package com.crud.react.serviceImpl;

import com.crud.react.DTO.LoginDto;
import com.crud.react.Service.UsersService;
import com.crud.react.enumed.UserType;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundexception;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users users = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response =  new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("users", users);
        return response;
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public Users addUsers(Users users) {
        String email = users.getEmail();
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        var validasi = usersRepository.findByEmail(email);
        if (users.getRole().name().equals("ADMIN"))
            users.setRole(UserType.ADMIN);
        else users.setRole(UserType.USER);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Email Already Axist");
        }
        return usersRepository.save(users);
    }


    @Override
    public Users getUsers(Integer Id) {
        return usersRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan")) ;
    }

    @Override
    public Users editUsers(Integer Id, String email, String password, UserType user_type) {
        return usersRepository.findById(Id).orElseThrow(() -> new NotFoundexception("id tidak ditemukan"));
    }

    @Override
    public void deleteUsersById(Integer Id) {
         usersRepository.deleteById(Id);
    }
}
